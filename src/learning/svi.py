from collections import defaultdict

import pyro
from pyro.infer import Trace_ELBO


def train_svi(model, guide, X, y=None, num_epochs=500, lr=1e-2, interval=50):
    pyro.clear_param_store()

    svi = pyro.infer.SVI(
        model=model,
        guide=guide,
        optim=pyro.optim.Adam({'lr': lr}),
        loss=pyro.infer.Trace_ELBO(),
    )

    history = {
        'losses': [],
        'params': defaultdict(list),
    }

    for epoch in range(num_epochs):
        loss = svi.step(X, y)
        history['losses'].append(loss)

        if interval and (epoch % interval == 0 or epoch + 1 == num_epochs):
            print(f'Epoch = {epoch}, Loss = {loss}')

        p = dict(pyro.get_param_store())
        for k, v in p.items():
            history['params'][k].append(v.detach().numpy().copy())

    return history
