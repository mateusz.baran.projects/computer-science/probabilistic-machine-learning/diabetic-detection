import matplotlib.pyplot as plt


def visualize_nb_parameters(history, save_path=None):
    num_epochs = len(history['losses'])
    losses = history['losses']
    params = history['params']

    plt.figure()
    plt.plot(range(num_epochs), losses)
    plt.xlabel('Epochs')
    plt.ylabel('Loss')
    plt.title('Loss')
    if save_path is not None:
        plt.savefig(save_path + 'loss.png')

    for pname, pvals in params.items():
        if pname == 'c_logits':
            plt.figure()
            for c in range(2):
                plt.plot(range(num_epochs), [v[c] for v in pvals], label=f'C={c}')
            plt.title('Class logits')
            plt.legend()
            if save_path is not None:
                plt.savefig(save_path + f'{pname}_c_logits.png')
        elif any(w in pname for w in ('_mu', '_sigma')):
            plt.figure()
            for c in range(pvals[0].shape[0]):
                vals = [v[c] for v in pvals]
                plt.plot(range(num_epochs), vals, label=f'{pname} | c={c}')
            plt.legend()
            plt.title(pname)
            if save_path is not None:
                plt.savefig(save_path + f'{pname}_num_params.png')
        else:
            fig, axs = plt.subplots(ncols=pvals[0].shape[1], figsize=(15, 5))
            for idx, ax in enumerate(axs):
                for c in range(pvals[0].shape[0]):
                    vals = [v[c][idx] for v in pvals]
                    ax.plot(range(num_epochs), vals, label=f'{pname}_(CAT={idx}) | c={c}')
                ax.legend()
            fig.suptitle(pname)
            if save_path is not None:
                plt.savefig(save_path + f'{pname}_cat_params.png')


def visualize_bayesian_network(history, save_path=None):
    num_epochs = len(history['losses'])
    losses = history['losses']
    params = history['params']

    plt.figure(figsize=(15, 5))
    plt.plot(range(num_epochs), losses)
    plt.xlabel('Epochs')
    plt.ylabel('Loss')
    plt.title('Loss')

    xs = range(num_epochs)

    for pname, pvals in params.items():
        plt.figure(figsize=(15, 5))

        if pname in ('theta_Pregnancies', 'theta_Age', 'theta_DPF', 'theta_BMI'):
            plt.plot(xs, pvals, label=f'P({pname} = 1)')
        elif pname == 'theta_HiddenOutcome':
            for p in (0, 1):
                for a in (0, 1):
                    for d in (0, 1):
                        vals = [v[p][a][d][0] for v in pvals]
                        plt.plot(xs, vals,
                                 label=f'P({pname} = 1 | Pregnancies = {p}, Age = {a}, DPF = {d}, BMI = 0)')
            plt.legend()
            plt.title(pname)
            if save_path is not None:
                plt.savefig(save_path + f'{pname}_2.png')
            plt.figure(figsize=(15, 5))
            for p in (0, 1):
                for a in (0, 1):
                    for d in (0, 1):
                        vals = [v[p][a][d][1] for v in pvals]
                        plt.plot(xs, vals,
                                 label=f'P({pname} = 1 | Pregnancies = {p}, Age = {a}, DPF = {d}, BMI = 1)')
        elif pname == 'theta_BloodPressure':
            for h in (0, 1):
                for b in (0, 1):
                    vals = [v[h][b] for v in pvals]
                    plt.plot(xs, vals, label=f'P({pname} = 1 | HiddenOutcome = {h}, BMI = {b})')
        elif pname == 'theta_SkinThickness':
            for b in (0, 1):
                vals = [v[b] for v in pvals]
                plt.plot(xs, vals, label=f'P({pname} = 1 | BMI = {b})')
        elif pname == 'theta_Glucose':
            for h in (0, 1):
                vals = [v[h] for v in pvals]
                plt.plot(xs, vals, label=f'P({pname} = 1 | HiddenOutcome = {h})')
        elif pname == 'theta_Insulin':
            for h in (0, 1):
                for g in (0, 1):
                    vals = [v[h][g] for v in pvals]
                    plt.plot(xs, vals, label=f'P({pname} = 1 | HiddenOutcome = {h}, Glucose = {g})')
        elif pname == 'theta_Outcome':
            for k in (0, 1):
                for i in (0, 1):
                    for g in (0, 1):
                        vals = [v[k][i][g] for v in pvals]
                        plt.plot(xs, vals,
                                 label=f'P({pname} = 1 | BloodPressure = {k}, Insulin = {i}, Glucose = {g})')
        plt.legend()
        plt.title(pname)
        if save_path is not None:
            plt.savefig(save_path + f'{pname}.png')
