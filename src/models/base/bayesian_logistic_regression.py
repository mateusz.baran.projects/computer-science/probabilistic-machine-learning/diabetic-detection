import pyro
import pyro.distributions as dist
from pyro.nn import PyroModule, PyroSample
from torch import nn


class BayesianLogisticRegression(PyroModule):
    def __init__(self, in_features):
        super().__init__()
        self.linear = PyroModule[nn.Linear](in_features, 1)
        self.linear.weight = PyroSample(
            dist.Normal(0., 1.).expand([1, in_features]).to_event(2)
        )
        self.linear.bias = PyroSample(
            dist.Normal(0., 10.).expand([1]).to_event(1)
        )

    def forward(self, x, y=None):

        mean = self.linear(x)
        with pyro.plate("data", x.shape[0]):
            return pyro.sample("obs", dist.Bernoulli(logits=mean.squeeze(-1)), obs=y)
