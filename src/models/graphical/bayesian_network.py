import pyro
import pyro.distributions as dist
import torch
from torch.distributions.constraints import unit_interval


class BayesianNetwork:
    @staticmethod
    def sample(name, distribution, obs):
        mask = torch.isnan(obs)
        valid = obs.clone()
        valid[mask] = 0
        samples = torch.empty_like(obs)

        with pyro.poutine.mask(mask=mask):
            samples[mask] = pyro.sample(name, distribution, obs=None)[mask]

        with pyro.poutine.mask(mask=~mask):
            samples[~mask] = pyro.sample(f'{name}_valid', distribution, obs=valid)[~mask]

        return samples

    def model(self, x, y=None):
        theta_P = pyro.param('theta_Pregnancies', torch.ones(1).div(2), constraint=unit_interval)
        theta_A = pyro.param('theta_Age', torch.ones(1).div(2), constraint=unit_interval)
        theta_D = pyro.param('theta_DPF', torch.ones(1).div(2), constraint=unit_interval)
        theta_B = pyro.param('theta_BMI', torch.ones(1).div(2), constraint=unit_interval)

        theta_H = pyro.param('theta_HiddenOutcome', torch.ones(2, 2, 2, 2).div(2),
                             constraint=unit_interval)

        theta_K = pyro.param('theta_BloodPressure', torch.ones(2, 2).div(2),
                             constraint=unit_interval)
        theta_S = pyro.param('theta_SkinThickness', torch.ones(2).div(2), constraint=unit_interval)

        theta_G = pyro.param('theta_Glucose', torch.ones(2).div(2), constraint=unit_interval)
        theta_I = pyro.param('theta_Insulin', torch.ones(2, 2).div(2), constraint=unit_interval)

        theta_O = pyro.param('theta_Outcome', torch.ones(2, 2, 2).div(2), constraint=unit_interval)

        with pyro.plate(f'plate', x.shape[0]):
            P = self.sample('Pregnancies', dist.Bernoulli(theta_P), obs=x.p).long()
            A = self.sample('Age', dist.Bernoulli(theta_A), obs=x.a).long()
            D = self.sample('DPF', dist.Bernoulli(theta_D), obs=x.d).long()
            B = self.sample('BMI', dist.Bernoulli(theta_B), obs=x.b).long()

            H = self.sample('HiddenOutcome', dist.Bernoulli(theta_H[P, A, D, B]), obs=x.o).long()

            K = self.sample('BloodPressure', dist.Bernoulli(theta_K[H, B]), obs=x.k).long()
            self.sample('SkinThickness', dist.Bernoulli(theta_S[B]), obs=x.s).long()

            G = self.sample('Glucose', dist.Bernoulli(theta_G[H]), obs=x.g).long()
            I = self.sample('Insulin', dist.Bernoulli(theta_I[H, G]), obs=x.i).long()

            return self.sample('Outcome', dist.Bernoulli(theta_O[K, I, G]), obs=x.o).long()

    def guide(self, x, y=None):
        pass
