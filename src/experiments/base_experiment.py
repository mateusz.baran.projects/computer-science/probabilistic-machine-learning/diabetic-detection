from sklearn.model_selection import StratifiedKFold


def k_fold_experiment(train_func, x, y, n_splits=5, shuffle=True, random_state=42):
    skf = StratifiedKFold(n_splits=n_splits, shuffle=shuffle, random_state=random_state)
    history = []

    for train_idx, test_idx in skf.split(x, y):
        x_train = x[train_idx]
        y_train = y[train_idx]
        x_test = x[test_idx]
        y_test = y[test_idx]
        history.append(train_func(x_train, y_train, x_test, y_test))

    return history

