import torch
from pyro.infer import Predictive


def summary(samples, confidence_factor=2):
    site_stats = {}
    for k, v in samples.items():
        mean = torch.mean(v, 0)
        std = torch.std(v, 0)
        sigma = confidence_factor * (std ** 2)
        site_stats[k] = {
            "mean": mean,
            "std": std,
            "lower_confidence": mean - sigma,
            "upper_confidence": mean + sigma,
            "confidence_range": 2 * sigma,
        }
    return site_stats


def predict(model, guide, x_test, return_sites, num_samples=1000, confidence_factor=2):
    predictive = Predictive(model, guide=guide, num_samples=num_samples, return_sites=return_sites)
    samples = predictive(x_test)
    return summary(samples, confidence_factor)
