import pandas as pd
import torch


def load_discrete_for_naive_bayes(path):
    df = pd.read_csv(path, index_col=0)

    for col in df.columns:
        df[col] = df[col].astype('category')

    return df[df.columns[:-1]], df[df.columns[-1]].to_numpy()


def load_continuous_for_naive_bayes(path):
    df = pd.read_csv(path, index_col=0)

    for col in df.columns[:-1]:
        df[col] = df[col].astype('float32')

    return df[df.columns[:-1]], df[df.columns[-1]].astype('category').to_numpy()


def load_continuous_for_bayesian_logistic_regression(path):
    df = pd.read_csv(path, index_col=0)

    for col in df.columns[:-1]:
        df[col] = df[col].astype('float32')

    X, y = df[df.columns[:-1]].to_numpy(), df[df.columns[-1]].astype('category').to_numpy()

    return torch.from_numpy(X).float(), torch.from_numpy(y).float()


def load_discrete_for_bayesian_network(path):
    df = pd.read_csv(path, index_col=0)
    df = df.astype('float32')

    return torch.from_numpy(df.to_numpy()).float()
