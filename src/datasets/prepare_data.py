from argparse import Namespace

import torch
import numpy as np


def prepare_data_for_bayesian_network(x_train, x_test):
    x_train = Namespace(
        p=x_train[:, 0],
        g=x_train[:, 1],
        k=x_train[:, 2],
        s=x_train[:, 3],
        i=x_train[:, 4],
        b=x_train[:, 5],
        d=x_train[:, 6],
        a=x_train[:, 7],
        o=x_train[:, 8],
        shape=x_train.shape
    )

    x_test = Namespace(
        p=x_test[:, 0],
        g=x_test[:, 1],
        k=x_test[:, 2],
        s=x_test[:, 3],
        i=x_test[:, 4],
        b=x_test[:, 5],
        d=x_test[:, 6],
        a=x_test[:, 7],
        o=torch.full(x_test[:, 8].shape, np.nan),
        O=x_test[:, 8],
        shape=x_test.shape
    )

    return x_train, x_test
